#coding in utf-8
'''
Projet NSI n°2 : Sur le chemin de Poudlard
Crée par Ahmed BENZEKHROUFA, jihed HELLA, Nil Superchi
Ce programme permet de tester differents rendus de monnaies à travers 3 magasins qui possèdent
chacun leurs tirroirs de caisse et le rendu de monnaies sera donc different pour chaque cas .

Auteurs : HELLA Jihed , BENZEKHROUFA Ahmed, SUPERCHI nil

Date de dernière révision : 06/01/2023

Lien du Gitlab : https://gitlab.com/nxl_.sb/harry-potter

'''


from tkinter import *

quantite_monnaie = [0, 2, 3, 1, 1, 1, 0, 5, 0]
monnaie = [500, 200, 100, 50, 20, 10, 5, 2, 1]


somme_a_rendre = int(input("Inserer une somme a rendre : "))
def libraire(somme_a_rendre, monnaie):
    """
    Fonction qui simule le rendu de monnaie de chez Fleury et Bott, le libraire, possedant un tirroir de caisse illimité
    Entrée: premier paramètre il y a un nombre entier que l'utilisateur insert/deuxieme parametre: une liste de monnaie 
    Sortie: renvoie une liste composé de l'argent rendu
    """
    pieces_rendues = []
    i = 0
    while somme_a_rendre > 0:
        if somme_a_rendre >= monnaie[i]:
            pieces_rendues.append(monnaie[i])
            somme_a_rendre -= monnaie[i]                
        else:
            i += 1

    return pieces_rendues


somme_a_rendre_2 = int(input("Inserer une somme a rendre pour la robe : "))
def robe(somme_a_rendre_2):
    """
    Fonction qui simule le rendu de monnaie de chez Madame Guipure possedant un tirroire de caisse limité
    Entrée: parametre il y a un nombre entier que l'utilisateur insert
    Sortie: renvoie une liste composé de l'argent rendu et d'une chaine de caractère
    """
    quantite_monnaie = [0, 2, 3, 1, 1, 1, 0, 5, 0]
    pieces_rendues = []
    monnaie = [500, 200, 100, 50, 20, 10, 5, 2, 1]
    i = 0  
    for i in range(len(monnaie)):
        while somme_a_rendre_2 >= monnaie[i] and quantite_monnaie[i] > 0:
            pieces_rendues.append(monnaie[i])
            somme_a_rendre_2 -= monnaie[i]  
            quantite_monnaie[i] -= 1           
        i += 1
    if somme_a_rendre_2 != 0:
        rendu_incomplet = 'rendu incomplet ' 
    else :
        rendu_incomplet = ''
    return [pieces_rendues, rendu_incomplet]

gallions = int(input("Inserer les gallions: "))
mornilles = int(input("Inserer les mornilles: "))
noises = int(input("Inserer les noises: "))
def fabriquant(gallions, mornilles, noises):
    """ 
    Fonction qui permet de convertir chaque pieces inseré par l'utilisateur
    Entrée: parametre: 3 nombres entiers
    Sortie: une liste des pièces rendus 
    """
    while noises >= 29 :
        difference = 29
        mornilles += 1 
        noises = noises - difference
    while mornilles >= 17 :
        difference = 17 
        gallions += 1 
        mornilles = mornilles - difference
    return [gallions, mornilles, noises]
     
def ouvrir_fenetre_1():
    """
    Procedure permettant d'afficher le rendu de monnaies à l'aide d'un champ
    """
    nouvelle_fenetre = Toplevel() #Ici l'instance Toplevel() est plus adapté à la situation d'ouvrir plusieurs fenêtre.
    nouvelle_fenetre.title("Chez le libraire")
    nouvelle_fenetre.minsize(480, 250)
    nouvelle_fenetre.config(background='#1a472a')
    
    hauteur, largeur = 70, 70
    
    image = PhotoImage(file="livre.png").zoom(35).subsample(32)
    canva = Canvas(nouvelle_fenetre, width=largeur, height=hauteur, bg='#1a472a', bd=0, highlightthickness=0)
    canva.create_image(largeur/2, hauteur/2, image=image)
    canva.grid(row=0,column=1, sticky=E)
    
    image_2 = PhotoImage(file="livre.png").zoom(35).subsample(32)
    canva_2 = Canvas(nouvelle_fenetre, width=largeur, height=hauteur, bg='#1a472a', bd=0, highlightthickness=0)
    canva_2.create_image(largeur/2, hauteur/2, image=image_2)
    canva_2.grid(row=0,column=3, sticky=W)
    
    global nouveau_champ
    nouveau_champ = Entry(nouvelle_fenetre, width=90, bg='white')
    nouveau_champ.grid(row=3, column=2, sticky=None)    
    nouveau_titre = Label(nouvelle_fenetre, text='bienvenue chez le libraire', font=("arial", 40), bg='#1a472a', fg='white')
    nouveau_titre.grid(row=0, column=2, sticky=None)

    boutton = Button(nouvelle_fenetre, text='Rendre monnaie', font=("arial", 40), bg='#1a472a', fg='white', command= libraire_affichage)
    boutton.grid(row=1, column=2, sticky=None)
                                              

    nouvelle_fenetre.mainloop()

def libraire_affichage():
    """
    Cette procédure va permettre d'inserer le rendu de monnaie dans la fenêtre correspondante
    """
    monnaie_libraire = libraire(somme_a_rendre, [500, 200, 100, 50, 20, 10, 5, 2, 1])
    print(monnaie_libraire)
    compteur = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(compteur)):
        compteur[i] = monnaie_libraire.count(monnaie[i])

        reponse = f" Je vous dois {compteur[0]} billet de 500, {compteur[1]} billet de 200, {compteur[2]} billet de 100, {compteur[3]} billet de 50, {compteur[4]} billet de 20, {compteur[5]} billet de 10, {compteur[6]} billet de 5, {compteur[7]} pièce de 2, {compteur[8]} pièce de 1. "
    
    nouveau_champ.delete(0, END) # Permet de supprimer les caractères inscrit dans le champs si on reclick sur le bouton.
    nouveau_champ.insert(0, reponse)


def ouvrir_fenetre_2():
    """
    Procedure permettant d'afficher le rendu de monnaies à l'aide d'un champ
    """
    nouvelle_fenetre = Toplevel()
    nouvelle_fenetre.title("Chez Madame Guipure")
    nouvelle_fenetre.minsize(480, 250)
    nouvelle_fenetre.config(background='#1a472a')
    
    hauteur, largeur = 90, 70
    
    image = PhotoImage(file="baguette-magique.png").zoom(35).subsample(75)
    canva = Canvas(nouvelle_fenetre, width=largeur, height=hauteur, bg='#1a472a', bd=0, highlightthickness=0)
    canva.create_image(largeur/2, hauteur/2, image=image)
    canva.grid(row=0,column=1, sticky=E)

    image_2 = PhotoImage(file="baguette-magique.png").zoom(35).subsample(75)
    canva_2 = Canvas(nouvelle_fenetre, width=largeur, height=hauteur, bg='#1a472a', bd=0, highlightthickness=0)
    canva_2.create_image(largeur/2, hauteur/2, image=image_2)
    canva_2.grid(row=0,column=3, sticky=W)
    
    global nouveau_champ_2
    nouveau_champ_2 = Entry(nouvelle_fenetre, width=90, bg='white', font=40)
    nouveau_champ_2.grid(row=3, column=2, sticky=None) 

    nouveau_titre = Label(nouvelle_fenetre, text='bienvenue chez Guipure', font=("arial", 40), bg='#1a472a', fg='white')
    nouveau_titre.grid(row=0, column=2, sticky=None)

    boutton = Button(nouvelle_fenetre, text='Rendre monnaie', font=("arial", 40), bg='#1a472a', fg='white', command= robe_affichage)
    boutton.grid(row=1, column=2, sticky=None)

    nouvelle_fenetre.mainloop()   

def robe_affichage():
    """
    Cette procédure va permettre d'inserer le rendu de monnaie limité par la caisse dans la fenêtre correspondante
    """
    monnaie_robe = robe(somme_a_rendre_2)
    print(monnaie_robe)
    compteur = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(compteur)):
        compteur[i] = monnaie_robe[0].count(monnaie[i])

        reponse = f"{monnaie_robe[1]} Je vous dois {compteur[0]} billet de 500, {compteur[1]} billet de 200, {compteur[2]} billet de 100, {compteur[3]} billet de 50, {compteur[4]} billet de 20, {compteur[5]} billet de 10, {compteur[6]} billet de 5, {compteur[7]} pièce de 2, {compteur[8]} pièce de 1. "                              

    nouveau_champ_2.delete(0, END)
    nouveau_champ_2.insert(0, reponse)
    
def ouvrir_fenetre_3():
    """
    Procedure permettant d'afficher le rendu de monnaies à l'aide d'un champ
    """
    nouvelle_fenetre = Toplevel()
    nouvelle_fenetre.title("Chez le fabriquant de baguettes magiques")
    nouvelle_fenetre.minsize(480, 250)
    nouvelle_fenetre.config(background='#1a472a')
    
    hauteur, largeur = 110, 70
    
    image = PhotoImage(file="cape_harry-removebg-preview.png").zoom(35).subsample(150)
    canva = Canvas(nouvelle_fenetre, width=largeur, height=hauteur, bg='#1a472a', bd=0, highlightthickness=0)
    canva.create_image(largeur/2, hauteur/2, image=image)
    canva.grid(row=0,column=1, sticky=E)

    image_2 = PhotoImage(file="cape_harry-removebg-preview.png").zoom(35).subsample(150)
    canva_2 = Canvas(nouvelle_fenetre, width=largeur, height=hauteur, bg='#1a472a', bd=0, highlightthickness=0)
    canva_2.create_image(largeur/2, hauteur/2, image=image_2)
    canva_2.grid(row=0,column=3, sticky=W)

    global nouveau_champ_3
    nouveau_champ_3 = Entry(nouvelle_fenetre, width=90, bg='white')
    nouveau_champ_3.grid(row=3, column=2, sticky=None) 

    nouveau_titre = Label(nouvelle_fenetre, text='bienvenue chez Ollivander', font=("arial", 40), bg='#1a472a', fg='white')
    nouveau_titre.grid(row=0,column=2, sticky=None)
    
    boutton = Button(nouvelle_fenetre, text='Rendre monnaie', font=("arial", 40), bg='#1a472a', fg='white', command= affichage_fabriquant)
    boutton.grid(row=1, column=2, sticky=None)

    nouvelle_fenetre.mainloop()

def affichage_fabriquant():
    """
    Cette procédure va permettre d'inserer les differentes pièces dans la fenêtre correspondante
    """
    monnaie_fabriquant = fabriquant(gallions, mornilles, noises)
    print(monnaie_fabriquant)
    
    reponse = f" Je vous dois {monnaie_fabriquant[0]} gallions, {monnaie_fabriquant[1]} mornilles, {monnaie_fabriquant[2]} noises " 
    
    nouveau_champ_3.delete(0, END)
    nouveau_champ_3.insert(0, reponse)

fenetre = Tk()
fenetre.title("Chemin de traverse")
fenetre.minsize(1080, 720)
fenetre.config (background='#1a472a')

titre = Label(fenetre, text='Bienvenue dans le chemin de traverse', font=("arial", 40), bg='#1a472a', fg='white')
titre.grid(row=0, column=0, sticky=E, pady=25, padx=(20, 20))

titre_1 = Label(fenetre, text='↓ Aller chez le libraire ↓', font=("arial", 40), bg='#1a472a', fg='white')
titre_1.grid(row=1, column=0, sticky='nwse')
boutton_1 = Button(fenetre, text='Entrez', font=("arial", 40), bg='#1a472a', fg='white', command=ouvrir_fenetre_1)
boutton_1.grid(row=2, column=0, sticky='nwse',pady=5, padx=40)

titre_2 = Label(fenetre, text='↓ Aller chez Madame Guipure ↓', font=("arial", 40), bg='#1a472a', fg='white')
titre_2.grid(row=3, column=0, sticky='nwse')
boutton_2 = Button(fenetre, text='Entrez', font=("arial", 40), bg='#1a472a', fg='white', command=ouvrir_fenetre_2)
boutton_2.grid(row=4, column=0, sticky='nwse', pady=5, padx=40)

titre_3 = Label(fenetre, text='↓ Aller chez le fabriquant de baguette ↓', font=("arial", 40), bg='#1a472a', fg='white')
titre_3.grid(row=5, column=0, sticky='nwse')
boutton_3 = Button(fenetre, text='Entrez', font=("arial", 40), bg='#1a472a', fg='white', command=ouvrir_fenetre_3)
boutton_3.grid(row=6, column=0, sticky='nwse', pady=5, padx=40)

fenetre.mainloop()

